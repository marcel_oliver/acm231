\documentclass[12pt]{article}
\usepackage{fullpage,amsmath,amsfonts,mathtools}

\def\d{{\mathrm d}}
\def\c{{\mathrm c}}
\def\nl{{\operatorname{nl}}}
\def\in{{\operatorname{in}}}
\def\out{{\operatorname{out}}}
\def\r{{\operatorname{right}}}
\def\l{{\operatorname{left}}}

\begin{document}

\title{Nonlinear Dynamics Lab}
\author{Session 14--16}
\date{due April 3, 2019}
\maketitle


\section{Chua's Circuit}


This weeks lab sessions study \emph{Chua's Circuit}, a nonlinear
electric oscillator which can be assembled with standard electronics
parts.  For background and circuit diagrams, consult the article by
Hobson and Lansbury \cite{HL96}.

One of the crucial ingredients for the circuit is the inductor $L$.
It is often necessary or convenient to use commodity inductors, which
are typically far from ideal.  In particular, real-world inductors
have a resistance $R_L$ which is typically non-negligible for the
operation of the Chua oscillator.  Other non-ideal effects include
parasitic capacitance, radiative losses and hysteresis which we shall
ignore to keep the modeling simple; however, these effects may still
be significant.

To include the effect of the inductor's resistance into the
mathematical description, we analyze it as a resistor in series with
an ideal inductor.  Consequently, the differential equations
describing Chua's Circuit read
\begin{align}
  C_1 \, \frac{\d V_1}{\d t}
  & = \frac{V_2 - V_1}{R_\c} - I_\nl (V_1) \,, \\
  C_2 \, \frac{\d V_2}{\d t}
  & = \frac{V_1 - V_2}{R_\c} + I_L \,, \\
  L \, \frac{\d I_L}{\d t}
  & = - V_2 - R_L \, I_L \,.
\end{align}
The resistance $R_\c$ is the coupling resistor, labeled $1/G$ in
Hobson and Lansbury's article.  A short derivation of these equations
should be contained in your lab report.

\section{Response curve of the inverse resistors}

The notation in the following computation refers to the left inverse
resistor in Figure~3 of \cite{HL96}.  Let $V_\in$ denote the voltage
at the input of the inverse resistor circuit, $V_\out$ the voltage at
the output of the operational amplifier.  First note that the  input
current $I_\in$ and the current through $R_3$ must coincide, so by
Ohm's law,
\begin{equation}
  V_\in - V_\out = R_3 \, I_\in \,.
  \label{e.eq1}
\end{equation}
Moreover, the current through $R_1$ and the current through $R_2$ must
coincide.  We now consider two cases.  First, if the operational
amplifier is not saturated, the voltage at its two inputs must be
identical.  Thus,
\begin{equation}
  \frac{U_\in}{R_1} = \frac{V_\out-V_\in}{R_2} \,.
  \label{e.eq2}
\end{equation}
Eliminating $V_\in-V_\out$ from \eqref{e.eq1} and \eqref{e.eq2}, we
obtain the response curve or transfer characteristic of an inverse
resistor,
\begin{equation}
  I_\in = - \frac{R_2}{R_1\,R_3} \, V_\in \,.
\end{equation}
Inserting this relation into \eqref{e.eq1}, we can also infer that
\begin{equation}
  V_\out = \frac{R_1 + R_2}{R_1} \, V_\in \,. 
  \label{e.voutvin}
\end{equation}

Second, the operational amplifier saturates when $V_\out = V_{\max}$.
According to \eqref{e.voutvin}, this occurs when
\begin{equation}
  V_\in \geq \frac{R_1}{R_1+R_2} \, V_{\max} \,.
\end{equation}
In this case, 
\begin{equation}
  I_\in = \frac{V_\in - V_{\max}}{R_3} \,.
\end{equation}
Altogether, for positive $V_\in$,
\begin{equation}
  I_\in =
  \begin{dcases}
    - \frac{R_2}{R_1\,R_3} \, V_\in &
    \text{for } 
    V_\in < \frac{R_1}{R_1+R_2} \, V_{\max} \,, \\
    \frac{V_\in - V_{\max}}{R_3} & 
    \text{otherwise} \,.
  \end{dcases}
\end{equation}
The expression for negative $V_\in$ is obtained by odd reflection.
The response curve $I_\nl(V_1)$ for the full negative resistor is
obtained by summing up the response currents for the two individual
negative resistors, with $V_\in = V_1$.

In the actual circuit, $R_1/(R_1+R_2) \approx 0.09$ is much less than
$R_4/(R_4+R_5) \approx 0.8$, so the left hand negative resistor
saturates easily while the right hand will not saturate at all.  Thus,
the output voltage $V_\r$ of the right-hand negative resistor is in a
one-to-one relationship with the input voltage $V_\in$ to both
negative resistors, so that, analogous to \eqref{e.voutvin},
\begin{equation}
  V_\in = \frac{R_4}{R_4+R_5} \, V_\r \,.
\end{equation}
The output voltage of the left-hand negative resistor is then given by
\begin{equation}
  V_\l = \min \biggl\{ V_{\max}, \frac{R_1 + R_2}{R_1} \, 
         \frac{R_4}{R_4+R_5} \, V_\r \biggr\} 
  \label{e.vl}
\end{equation}
for positive $V_\r$; the formula for negative $V_\r$ is once again
obtained by odd reflection.

\section{Preparatory tasks}

\emph{Note}: Everybody should have made significant progress with the
following tasks before the experimental session.  They will be part of
the lab report, to be submitted in groups of two.  Code-sharing within
the group is permitted.  However, it is expected that every
participant is familiar enough with the task that he/she could have
written the code on their own and is able to run, modify, and explain
the code on request.

\begin{enumerate}
\item Define a function for the nonlinear resistor whose transfer
characteristic $I_\nl(V)$ is depicted in Figure~2 of Hobson and
Lansbury \cite{HL96}.  (E.g., define parameters for the slopes and the
location of the kink.)

\item Code up a solver for the Chua system, either by modifying your
hand-coded solver from the second code assignment, or by using one of
the build-in solvers from Scipy.  (The latter is likely more robust and
faster.)  For values of the various parameters, see below.

\item Something to think about: what should you plot as the output of
your simulation in order to replicate the picture seen on the $X$-$Y$
oscilloscope attached to the circuit as depicted in \cite{HL96}?

\end{enumerate}


\section{Lab tasks}

\begin{enumerate}
\item The inductor we have readily available is one with $10 \,
\mathrm{mH}$. Take a multimeter and measure its resistance $R_L$.

\item Assemble Chua's circuit.  Since the inductor is different from
the one used in \cite{HL96}, some resistances should be chosen
differently from the circuit diagram: take
$R_1 = 2.2 \, \mathrm{k\Omega}$ and $R_4 = 1 \,\mathrm{k\Omega}$, the
other values as in the diagram.  For the variable resistance $R_\c$ we
use a multiturn potentiometer which allows fine control of its
resistance.

\item Determine experimentally the value of $R_\c$ which corresponds
to the onset of chaos.  Do you see a sharp transition, or a period
doubling cascade as for the logistic map?

\item Tune the circuit back into a regime where it is oscillating
stably with larger amplitude.  Switch the oscilloscope into
$Y$-$T$-mode, attach the two channel probes to the outputs of the two
operational amplifiers, and use the oscilloscope ``save'' function to
store a time series sample of both channel inputs.  This data is used
later to verify the theoretical response curve of the nonlinear
resistor.

\item If you have time: Use an inductor with $100 \, \mathrm{mH}$,
also available in the lab, and try if you can---potentially modifying
the values of the capacitors or resistors as well---find a regime as
well.  If this is successful, you will likely obtain less hysteresis
in the response curve of the nonlinear resistors, and consequently
better agreement of theory and experiment.  (Note: the $100 \,
\mathrm{mH}$ inductor might have too big a resistance to excite
nonlinear oscillations, so this is not guaranteed to work!)

\end{enumerate}

\section{Report items}

\begin{enumerate}

% \item Analyze the ``inverse resistor'' consisting of $R_1$, $R_2=R_3$,
% and the operational amplifier.  Write out an expression for the
% current response as a function of the input voltage, assuming that the
% operational amplifier saturates at output voltage $\pm V_{\max}$.
% Write out and plot an expression for the overall response curve of the
% two parallel inverse resistors used in Chua's circuit.

\item Use the channel time series obtained during the experiment to
verify the time series of the nonlinear resistivity.  You may proceed
as follows.  Take the time series of the channel voltages of the
right-hand operational amplifier and use them as input for equation
\eqref{e.vl} to compute a \emph{predicted} output voltage of the
left-hand operational amplifier.  (How do you determine $V_{\max}$?)
Plot the predicted and the measured value of $V_\l$ vs.\ $V_\r$ in one
coordinate system.  Discuss any deviations you might see.

\item Write a program which simulates Chua's circuit, using the
theoretical response curve, over a time interval
$T=0.02 \, \mathrm{s}$.  Plot $V_1$ vs.\ $V_2$ for times $t=[T/2,T]$,
thereby discarding transients.  Find the value for $R_\c$ at the onset
of chaos.  

\item Compare the experimental with the numerical results and discuss
possible differences.

\item Challenge question (not required for full score): Convert your
measured voltage data of the nonlinear resistance into a response
curve and use it instead of the theoretical curve in your simulation.
Do you get better agreement between theory and experiment using the
measured response curve?

\end{enumerate}

The experiment and the lab report may be done in groups of two.



\section{Parts list}

\begin{itemize}
\item 1 Capacitor $10\,\mathrm{nF}$
\item 3 Capacitors $100\,\mathrm{nF}$
\item 1 Inductor $10 \, \mathrm{mH}$ 
\item 2 Resistors $220 \, \mathrm{\Omega}$ (rd-rd-bk-bk-br)
\item 1 Resistor $1 \, \mathrm{k\Omega}$ (br-bk-bk-br-br)
\item 1 Resistor $2.2 \, \mathrm{k\Omega}$ (rd-rd-bk-br-br)
\item 2 Resistors $22 \, \mathrm{k\Omega}$ (rd-rd-bk-rd-br)
\item 2 Operational Amplifiers LM741
\item 1 Multi-turn potentiometer $1 \, \mathrm{k\Omega}$
\end{itemize}

\begin{thebibliography}{99}
\bibitem{HL96}
  P.R. Hobson and A.N. Lansbury,
  \textit{A simple electronic circuit to demonstrate bifurcation and
  chaos}, 
  Phys. Educ. \textbf{31} (1996), 39--43. \\
  


\end{thebibliography}




\end{document}

