\documentclass[12pt]{article}
\usepackage{fullpage,amsmath,amsfonts,url}
\usepackage[version=3]{mhchem}

\def\d{{\mathrm d}}
\def\c{{\mathrm c}}
\def\nl{{\operatorname{nl}}}

\begin{document}

\title{Nonlinear Dynamics Lab}
\author{April 30, 2019}
\date{Report due May 14, 2019}
\maketitle

\section*{Tasks}


\begin{enumerate}

\item Take the concentration series given in
Table~\ref{t.bz1}--\ref{t.bz2} below.  Derive a table with the
corresponding values for the concentrations $A$, $B$, $C$, and $H$
(these are assumed constant during the experiment) and for the initial
values of the time dependent concentrations $X$, $Y$, and $Z$.

\item Solve the system of differential equations \eqref{e.oregonator}
for each of the experiments and measure the period of oscillations.
(This can be done manually from a time series plot as high accuracy is
not required.)  Compare to the the experimentally measured periods.
Comment on your results.

\emph{Alternative task:} Obtain your period from a relaxation
oscillation analysis rather than a direct numerical simulation and
proceed as above.

\end{enumerate}
Your work should be written up as a very brief report.  For the
simulation/analysis, take the reaction rate constants reported in
Table~\ref{t.constants} using the values from RZ84, however with $q=1$
instead of the value listed.  (You may alternatively perform this task
on the values from MSR91.)

\section*{Modeling the Belousov--Zhabotinskii reaction}

\begin{table}
\centering
\begin{tabular*}{\textwidth}%
{@{}c@{\extracolsep{\fill}}c@{\extracolsep{\fill}}c@{}}
\hline
\strut\textbf{Name of Compound} & \textbf{Variable} & \textbf{Assumption} \\
\hline
\smash[b]\strut
Bromate & $A = [\ce{Br O_3-}]$ & Adiabatic \\
Organic compounds & $B = [\ce{Org}]$     & Adiabatic \\
Oxidized organic compounds & $B^*= [\text{Ox1}]$  & Quasi-static \\
Total iron & $C = [\ce{Fe^{2+}}]+[\ce{Fe^{3+}}]$ & Constant \\
Hydrogen ions & $H = [\ce{H+}]$ & Buffered \\
Bromine dioxide & $U = [\ce{Br O_2}]$ & Quasi-static \\
Bromous acid & $X = [\ce{H Br O_2}]$ &  \\
Bromide & $Y = [\ce{Br-}]$ & \\
Iron(III) & $Z = [\ce{Fe^{3+}}]$ &  \smash[t]\strut \\
\hline
\end{tabular*}
\caption{List of chemical compounds}
\end{table}

The mathematical modeling of the Belousov--Zhabotinskii reaction was
pioneered by Field, K\"or\"os and Noyes
\cite{FieldKN:1972:OscillationsCS} who proposed what is now known as
the FNK mechanism.  We use the adaptation to the ferroin catalyzed
variant of the reaction proposed by Rovinsky and Zhabotinskii
\cite{RovinskyZ:1984:MechanismMM} with simplification and in the
notation of \cite{TaylorVJ:1999:AnalysisRD}, which leads to the
following system of rate equations:
\begin{gather}
  \dot X = -k_1 \, H X Y + k_2 \, H^2 A Y
     - 2 \, k_3 \, X^2 - k_4 \, HAX + 2 \, k_{-4} \, U^2
     + k_5 \, HU (C-Z) \,, \\
  \dot Y = -k_1 \, HXY - k_2 \, H^2 A Y 
     + q \, k_7 \, B^* \\
  \dot Z = -k_6 \, BZ + k_{-6} \,  (C-Z) \, H B^* + k_5 \, HU \, (C-Z)
  \\
  \dot U = 2 \, k_4 \, HAX - 2 \, k_{-4} \, U^2 - k_5 \, HU \, (C-Z) \\
  \dot B^* =  k_6 \, BZ - k_{-6} \,  (C-Z) \, H B^* - k_7 \, B^*
\end{gather}
The reaction constants $k_6$, $k_{-6}$, and $k_7$ are not individually
known, but it is known that the $B^*$-equation is rapid, hence we may
assume that it is in a quasi-steady state where
\begin{gather}
  k_6 \, BZ - k_{-6} \, (C-Z) \, H B^* = k_7 \, B^*
\end{gather}
and therefore 
\begin{gather}
  B^* = \frac{k_6 \, BZ}{k_{-6} \, (C-Z) H + k_7}
      \approx \frac{k_6 \, BZ}{k_{-6} \, (C-Z) H} \,.
\end{gather}
Similarly, it is known that the $U$-equation is rapid and that the
term $k_{-4} \, U^2$ is small compared to the others.  Hence,
neglecting this term and assuming quasi-stationarity gives 
\begin{gather}
   k_5 \, HU \, (C-Z) = 2 \, k_4 \, HAX \,.
\end{gather}
Altogether,
\begin{subequations}
  \label{e.oregonator}
\begin{gather}
  \dot X = -k_1 \, H X Y + k_2 \, H^2 A Y
     - 2 \, k_3 \, X^2 + k_4 \, HAX \\
  \dot Y = -k_1 \, HXY - k_2 \, H^2 A Y 
     + q \, \frac{k_J \, B}H \, \frac{Z}{C-Z} \\
  \dot Z = - \frac{k_J \, B}H \, \frac{Z}{C-Z} + 2 \, k_4 \, HAX 
\end{gather}
\end{subequations}
where
\begin{gather}
  k_J = \frac{k_6 \, k_7}{k_{-6}} \,.
\end{gather}
Values for all the constants from the literature are listed in
Table~\ref{t.constants}.

This system can be further simplified and analyzed as a relaxation
oscillation; see, e.g., \cite{Gray:2002:AnalysisBZ}.


\begin{table}
\centering
\begin{tabular*}{\textwidth}%
{@{}c@{\extracolsep{\fill}}c@{\extracolsep{\fill}}c@{}}
\hline
\strut\textbf{Constant} 
  & \textbf{RZ84} \cite{RovinskyZ:1984:MechanismMM} 
  & \textbf{MSR91} \cite{MoriSR:1991:ProfilesCW} \\
\hline
\smash[b]\strut$k_1$ 
  & $k_5=10^7\,\mathrm{M^{-2}}\,\mathrm{s}^{-1}$
  & $k_5=10^7\,\mathrm{M^{-2}}\,\mathrm{s}^{-1}$ \\

$k_2$ 
  & $k_7/H, k_7 = 15\,\mathrm{M^{-2}}\,\mathrm{s}^{-1}$ 
  & $k_7 = 2\,\mathrm{M^{-2}}\,\mathrm{s}^{-1}$ \\

$k_3$ 
  & $k_4\,H, k_4 = 1.7 \times 10^4 
    \, \mathrm{M^{-2}}\,\mathrm{s}^{-1}$
  & $k_4 = 2 \times 10^3 \, \mathrm{M^{-1}}\,\mathrm{s}^{-1}$ \\

$k_4$ 
  & $k_1=100 \, \mathrm{M^{-2}}\,\mathrm{s}^{-1}$
  & $k_1=40 \, \mathrm{M^{-2}}\,\mathrm{s}^{-1}$ \\

$k_{-4}$ 
  & $k_{-1}/2, k_{-1} = 1.2 \times 10^5 \,
    \mathrm{M}^{-1} \, \mathrm{s}^{-1}$
  & $k_{-1}/2, k_{-1} = 4 \times 10^7 \,
    \mathrm{M}^{-1}\,\mathrm{s}^{-1}$ \\

$k_5$ 
  & $k_3/H, k_3=1.2 \times 10^5 \,\mathrm{M}^{-1}\,\mathrm{s}^{-1}$
  & $k_3=10^7\, \mathrm{M}^{-1}\,\mathrm{s}^{-1}$\\

$k_J$
  & $K_8 = 2 \times 10^{-5} \, \mathrm{M} \,\mathrm{s}^{-1}$
  & $K_8 = 3 \times 10^{-6} \, \mathrm{M} \,\mathrm{s}^{-1}$ \\

$q$ 
  & $q = 0.5$ (better value: $q=1$)
  & $h = 1.3$ \smash[t]{\strut}\\

\hline
\end{tabular*}
\caption{Two sets of values for the various constants from Rovinsky and
Zhabotinskii \cite{RovinskyZ:1984:MechanismMM} and Mori, Schreiber, and
Ross \cite{MoriSR:1991:ProfilesCW}.  The left hand column refers to
our naming of constants.  The other columns refer to the names of the
constants in the respective papers.  Note that sometimes the inclusion
of stochiometric factors and of $H=[\ce{H+}]$ is handled differently
in the different papers.  The values are listed so that the
expressions in each row can be identified.  RZ84 quote values of $H$
between $2\,\mathrm{M}$ and $2.5\,\mathrm{M}$, but it is not clear
which value was used when estimating the constants.  RZ84 state that
they re-estimated their constants $k_{\pm1}$ to $k_{\pm5}$ as older
values in the literature do not apply to the Ferroin-catalyzed BZ
reaction.  The values in MSR91 are partially
quoted from earlier literature and are therefore not necessarily more
reliable than the RZ84 values.  Finally, the units for the reaction
constants in RZ84 are either wrong (first two table rows) or entirely
missing (all the others), they are corrected in this table on the
assumption that this was a simple editorial oversight.
\label{t.constants}}
\end{table}


\section*{Experimental results}

To perform the experiment, four solutions are prepared:
\begin{description}
\item[Solution 1:] $0.5\,\mathrm{M}$ sodium bromate and
$0.5\,\mathrm{M}$ sulfuric acid

% $6\,\mathrm{g}$ sodium bromate (\ce{Na Br O_4}) in
% $80\,\mathrm{m}\mathrm{l}$ water, add $2.3\,\mathrm{m}\mathrm{l}$
% concentrated sulphuric acid (\ce{H_2 S O_4}).

\item[Solution 2:] $1\,\mathrm{M}$ malonic acid

%$2\,\mathrm{g}$ malonic acid with $20\,\mathrm{m}\mathrm{l}$ water.

\item[Solution 3:] $1\,\mathrm{M}$ sodium bromide 

%$1.2\,\mathrm{g}$ sodium bromide with $12\,\mathrm{m}\mathrm{l}$ water.

\item[Solution 4:] $0.025\,\mathrm{M}$ ferroin solution

%Add $0.88\,\mathrm{g}$ 1,10-phenanthroline monohydrate and
%$0.35\,\mathrm{g}$ iron(II) sulphate heptahydrate (\ce{Fe S O_4 . 7
%H_2 O}) to $50\,\mathrm{m}\mathrm{l}$ water.

%This receipe is close to Winfree 1972

\end{description}
Table~\ref{t.bz1}--\ref{t.bz3} show three measurement sequences where
the period is reported as a response to changing the volume fraction
of exactly one of the solutions.

When comparing with simulation data, the concentrations of each of the
reactants in Solutions~1--4 must be weighted with the volume fraction
of each solution in the final mixture.  The concentration
$H=[\ce{H+}]$ should be taken as the concentration of \ce{H_2 S O_4},
because within the range of concentrations used here, only one of the
two hydrogen ions is dissociating from the compound, thus freely
available.


\begin{table}[p]
\begin{center}
\begin{tabular}{c@{~~~~}c@{~~~~}c@{~~~~}c@{~~~~}c@{~~~~}c}
\hline
\textbf{Sol.\ 1} \strut &
\textbf{Sol.\ 2} &
\textbf{Sol.\ 3} &
\textbf{Sol.\ 4} &
\textbf{Water} &
\textbf{Period} $[\mathrm{s}]$ \\
\hline
$800\vphantom{1_1^1}$ &  &  &  & 50 & 6.6 \\
$700$ &  &  &  & 150 & 21.1 \\
$600$ & 100  & 50  & 25 & 250 & 76.6 \\
$500$ &  &  &  & 350 & 137.1 \\
$400$ &  &  &  & 450 & 182.8 \\
\hline
\end{tabular}
\end{center}
\caption{Measurement sequence varying the concentration of bromate and
sulfuric acid (Solution~1).  In the actual lab experiments, the
volumes are measured in $\mu \mathrm{l}$.\label{t.bz1}}
\end{table}

\begin{table}[p]
\begin{center}
\begin{tabular}{c@{~~~~}c@{~~~~}c@{~~~~}c@{~~~~}c@{~~~~}c}
\hline
\textbf{Sol.\ 1} \strut &
  \textbf{Sol.\ 2} &
  \textbf{Sol.\ 3} &
  \textbf{Sol.\ 4} &
  \textbf{Water} &
  \textbf{Period} $[\mathrm{s}]$ \\
\hline
$\vphantom{1_1^1}$ & 150 &  &  & 0 & 12.9 \\
$$ & 125 &  &  & 25 & 8.8 \\
$800$ & 100  & 50  & 25 & 50 & 8.8 \\
$$ & 75 &  &  & 75 & 6.6 \\
$$ & 50 &  &  & 100 & 31 \\
\hline
\end{tabular}
\end{center}
\caption{Measurement sequence varying the concentration of malonic
acid (Solution~2).\label{t.bz2}}
\end{table}

\begin{table}[p]
\begin{center}
\begin{tabular}{c@{~~~~}c@{~~~~}c@{~~~~}c@{~~~~}c@{~~~~}c}
\hline
\textbf{Sol.\ 1} \strut &
\textbf{Sol.\ 2} &
\textbf{Sol.\ 3} &
\textbf{Sol.\ 4} &
\textbf{Water} &
\textbf{Period} $[\mathrm{s}]$ \\
\hline
$\vphantom{1_1^1}$ &  & 100  &  & 0 & 76.4 \\
$$ &  & 80 &  & 20 & 38.5 \\
$800$ & 100  & 60  & 25 & 40 & 7.9 \\
$$ &  & 40 &  & 60 & 5.8 \\
$$ &  & 20  &  & 80 & 19.8 \\
\hline
\end{tabular}
\end{center}
\caption{Measurement sequence varying the concentration of bromide
(Solution~3).\label{t.bz3}}
\end{table}



\begin{thebibliography}{1}

\bibitem{TaylorVJ:1999:AnalysisRD}
{\sc A.~F.~Taylor, V.~Gaspar, B.~R.~Johnson, and S.~K.~Scott}, {\em Analysis of
  reaction-diffusion waves in the ferroin-catalysed {B}elousov--{Z}habotinsky
  reaction}, Phys. Chem. Chem. Phys., 1 (1999), pp.~4595--4599.

\bibitem{FieldKN:1972:OscillationsCS}
{\sc R.~J. Field, E.~K\"or\"os, and R.~M. Noyes}, {\em Oscillations in chemical
  systems. {II}. {T}horough analysis of temporal oscillation in the
  bromate-cerium-malonic acid system}, J. Am. Chem. Soc., 94 (1972),
  pp.~8649--8664.

\bibitem{Gray:2002:AnalysisBZ}
{\sc C.~R. Gray}, {\em An analysis of the {B}elousov--{Z}habotinskii reaction},
  Rose-Hulman Undergrad. Math J., 3 (2002), pp.~1--15.

\bibitem{MoriSR:1991:ProfilesCW}
{\sc E.~Mori, I.~Schreiber, and J.~Ross}, {\em Profiles of chemical waves in
  the ferroin-catalyzed {B}elousov--{Z}habotinskii reaction}, J. Phys. Chem.,
  95 (1991), pp.~9359--9366.

\bibitem{RovinskyZ:1984:MechanismMM}
{\sc A.~Rovinsky and A.~Zhabotinsky}, {\em Mechanism and mathematical model of
  the oscillating bromate-ferroin-bromomalonic acid reaction}, J. Phys. Chem.,
  88 (1984), pp.~6081--6084.

\end{thebibliography}


\end{document}

