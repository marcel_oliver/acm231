\documentclass[a4paper,11pt]{article}

% required packages
\usepackage[utf8x]{inputenc}
\usepackage{newtxtext,newtxmath}
\usepackage{paralist,html}
\usepackage{color}

\newcommand\blfootnote[1]{%
  \begingroup
  \renewcommand\thefootnote{}\footnote{#1}%
  \addtocounter{footnote}{-1}%
  \endgroup
}

%opening
\title{\textcolor{blue}{110231/110233 Applied Dynamical Systems + Lab}}
\author{Marc-Thorsten H\"utt and Marcel Oliver}
\date{\today}

\begin{document}

\maketitle


\section{Course description}

Applied Dynamical Systems together with the associated Lab serves as a
first problem-oriented introduction to nonlinear dynamics. It also
provides the foundation for the third year specialization courses
\emph{Differential Equations} and \emph{Dynamical Systems and
Control}. Nonlinear phenomena will be explored both in the laboratory
and on the computer. The lab will cover real-world examples of
nonlinear dynamics experiments such as nonlinear electric oscillators
and pattern formation in chemical reactions, as well as some
paradigmatic models of nonlinear dynamics.

Programming environments will be Scientific Python for number
crunching and Mathematica for symbolic computing.

A main focus of the lab is the development of computational
experiments for nonlinear phenomena, the application of automated
tools for bifurcation analysis, and continuation methods.  We will
also implement simple agent-based models and pseudo-spectral PDE
solvers for reaction-diffusion equations.

%Students wishing to hear about the theoretical foundations of some of
%the topics simultaneously are suggested to also take \emph{Applied
%Differential Equations and Modeling}, \emph{Ordinary Differential
%Equations and Dynamical Systems} and/or \emph{Applied Stochastic
%Processes}.

\section{Grading}

The course grade is a portfolio grade derived from 
\begin{itemize}
\item 5 Code Assignments with weight 1 and
\item 5 Lab Reports with weight 2 each.
\end{itemize}
Participants should be prepared to present their report and/or program
code to the instructor or the entire class; this presentation may be
part of the grading scheme of a particular report or code assignment.

The three lowest-graded units of work are dropped from the grade
computation, so the grade is computed from 12 units in total.  This
rule covers all minor emergencies including minor illness and
extracurricular activities.  Further exceptions will only be made in
truly extenuating and well-documented circumstances.

Lab reports and code assignments are due within \textbf{five calendar
days} after the day they are assigned, see syllabus below.  

\section{Students registered for the ``Lecture'' only}

Students registered for the \emph{Applied Mathematics} module must
take the lecture (CO18-110231) and the lab (CO18-110233).  The grading
scheme results in a single grade for both components.

Graduate students who take this class as part of their methods
education and who register for 5 ECTS only (course number
MMM008-110231) need to attend sessions from the schedule given below
and submit the associated assignments for at least 8 units and at most
10 units of work as defined above.  In this case, students are
\emph{required} to talk to one of the instructors beforehand and
declare which work will be submitted.

\section{Submission}

Code assignments and lab reports must be submitted via \textsf{git}
unless specified otherwise for a particular assignment.  Students are
required to set up \textsf{git} as described in the section ``Set up
steps for students'' in this document:
\begin{center}
  \url{https://bitbucket.org/marcel_oliver/git_for_academics}
\end{center}
The URL for the bitbucket course web page is 
\begin{center}
  \url{https://bitbucket.org/marcel\_oliver/acm231}
\end{center}
and the SSH URL which you will need to clone the repository is
\begin{center}
  \texttt{https://bitbucket.org/marcel\_oliver/acm231}
\end{center}

\section{Guidelines for code submissions}

You need to submit your code in one or more self-contained program
files so that the code can be run from the command line \emph{without
user intervention}, producing the requested output.  If you are asked
to answer short questions about the code or about the result, your
answers should be submitted in a separate file, or (if appropriate) as
source code comments in the program file.

Your code should be well-commented to explain the purpose of each
non-trivial step of your program.  The code structure must be clear;
variables and functions must be sensibly named.  Inadequately
commented or unnecessarily intransparent code will result in a lower
grade.

When submitting with git, place all files into the director which
contains the corresponding task sheet.  For Python assignments,
\emph{do not} use \textsf{Jupyter} notebooks as they cannot be easily
run from the command line and they cannot be diff'ed transparently.


\section{Guidelines for lab protocols}

Lab reports must be accompanied by all computer code used to generated
the results; see the section above for code submissions.

The report must be typeset in {\LaTeX } and submitted electronically
as a single PDF document; when submitting with git, the source file
must be submitted as well.
 
Reports should be as brief as possible (3--4 pages can sometimes be
enough) without omitting essential information or discussions.  You
don't need to write a text book!

The structure of the lab protocol resembles that of a short scientific paper: 
\begin{enumerate}
\item Title
\item Author
\item Abstract (briefly summarizing the goal and the result in 3-5 sentences)
\item Introduction (including a brief summary of the mathematical and
scientific background, as well as the history of the problem, if
applicable)
\item Methods (including details about the implementation, programming
strategy; in the ``Methods'' section you should describe exactly what
you have been doing: which algorithm, what parameters, experimental
setup, what postprocessing/visualization of the data, etc.)
\item Results 
\item Conclusion (summarizing your findings and embedding them in a broader perspective)
\item References
\end{enumerate}

All references, online or paper, must be included in the list of
references. All help received must be acknowledged, including code
segments taken from peers. There is no grade penalty for properly
credited help, as long as the submission as a whole remains in essence
an independent original contribution. Help or ``borrowed code''
without attribution is plagiarism and will be pursued according to the
Code of Academic Integrity.

\newpage

\section{Structure of the course (subject to change)}

\begin{itemize}
\item Tuesday sessions: 8:15--11:00 in West Hall 8
\item Thursday sessions: 15:45--17:00 in East Hall 4
\end{itemize}

\noindent
\begin{tabular}{@{}lllll}
\textbf{\textcolor{blue}{Date}}         &
\textbf{\textcolor{blue}{Instr.}}       &       
\textbf{\textcolor{blue}{Task}}       &       
\textbf{\textcolor{blue}{Topic}}        \\
Tu, 5. Feb.  & MO  &        & Introduction I, Logistic Map I \\
Th, 7. Feb.  & MO  &        & Logistic Map II \\
Tu, 12. Feb. & MO  & Code   & Logistic Map III, ODE solvers I  \\
Th, 14. Feb. & MTH &        & Introduction II  \\
Tu, 19. Feb. & MTH &        & Programming intro (Mathematica) \\
Th, 21. Feb. & MTH & Code   & Programming intro (Mathematica) \\
Tu, 26. Feb. & MO  &        & ODE solvers II \\
Th, 28. Feb. & MO  & Code   & ODE solvers III \\
Tu, 5. Mar.  & MTH &        & Stability analysis of a simple neuron model \\
Th, 7. Mar.  & MTH & Code   & Basic principles of spiral wave formation \\
Tu, 12. Mar. & TP  &        & Finite difference discretization of 1D steady state diffusion \\
Th, 14. Mar. & TP  &        & Diffusion equation \\                                              
Tu, 19. Mar. & TP  & Report & Simple reaction-diffusion model \\                                  
Th, 21. Mar. & MO  &        & Chua's circuit: Theory \\                  
Tu, 26. Mar. & MO  &        & Chua's circuit: Experiment \\              
Th, 28. Mar  & MO  & Report & Chua's circuit: Computational discussion \\
Tu, 2. Apr.  & --  &        & Homework session \\
Th, 4. Apr.  & AM  &        & Daisyworld I -- Biological homeostasis in an idealized world \\    
Tu, 9. Apr.  & AM  & Report & Daisyworld II \\                                                   
Th, 11. Apr. & MTH &        & Biochemical switches and oscillators I \\ 
Tu, 23. Apr. & MTH &        & Biochemical switches and oscillators II \\
Th, 25. Apr. & MTH & Report & Excitable dynamics in networks \\
Tu, 30. Apr. & --  &        & Homework session \\
Th, 2. May   & MO  &        & Belousov-Zhabotinsky reaction: Experiment \\
Tu, 7. May   & MO  & Report & Belousov-Zhabotinsky reaction: Theory \\
Th, 9. May   & MTH &        & Cellular automata: Wolfram classes \\
Tu, 14. May  & MTH & Code   & Cellular automata: Edge of chaos \\
Th, 16. May  & --  &        & Homework session
\end{tabular}
\bigskip

\noindent\emph{Note:} When a task is assigned, you have \textbf{five}
more days to work on it, it is due by midnight of the last of these
five days.


\end{document}
